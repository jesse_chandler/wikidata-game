#!/bin/bash
cd /data/project/wikidata-game/tools
./update_genderless_people.php
echo "select term_text,group_concat(distinct term_entity_id separator '|') AS items,count(distinct term_entity_id) AS cnt from wb_terms where term_entity_type='item' and term_type in ('label','alias')  group by term_text having cnt>1" | /usr/bin/sql wikidatawiki_p | gzip -c > duplicate_labels.gz
zcat duplicate_labels.gz | ./update_merge_candidates.php
./update_people_candidates.php
